# Python 구문 기초: 변수, 데이터 타입

## 소개
Python 구문 기초에 오신 것을 환영한다. 이 포스팅에서는 Python에서 변수를 선언하고 다양한 데이터 타입을 사용하는 방법을 배울 것이다. 변수는 모든 프로그래밍 언어의 구성 요소이며 데이터 타입은 변수가 저장할 수 있는 값의 범주이다. 이 글이 끝날 때까지 다음을 공부할 것이다.

- Python에서 변수가 무엇이고 어떻게 작동하는지 이해한다
- 다른 방법을 사용하여 변수에 값을 선언하고 할당한다
- Python에서 숫자, 문자열, 부울, 리스트, 튜플, 사전 및 집합과 같은 다양한 데이터 타입을 사용한다
- 다양한 데이터 타입에 대한 기본 작업과 조작 수행

이 글은 Python과 데이터 분석에 대한 기본적인 이해를 가지고 있다고 가정한다. 복습이 필요한 경우 이 [Python for Data Analysis](https://wesmckinney.com/book/) 과정을 다시 한 번 읽도록 하세요. 이 포스팅의 코드 예를 실행하려면 Python 인터프리터와 온라인 코드 편집기가 필요하다. 이 [online Python editor](https://www.online-python.com/#google_vignette) 또는 원하는 다른 도구를 사용할 수 있다.

시작할 준비가 되었나요? Python 구문의 기초에 대해 살펴보겠다!

## 변수란?
변수는 모든 프로그래밍 언어에서 가장 기본적인 개념 중 하나이다. 변수는 컴퓨터의 메모리에 저장된 값을 나타내는 이름이다. 변수를 프로그램에서 사용할 수 있는 데이터를 저장하는 용기라고 생각할 수 있다. 예를 들어, `name`이라는 변수를 만들어 `"Alice"`라는 값을 지정할 수 있다. 그런 다음 변수 `name`을 사용하여 프로그램에서 `"Alice"`라는 값을 액세스할 수 있다.

```python
# Create a variable called name and assign it the value "Alice"
name = "Alice"

# Print the value of the variable name
print(name)
```

위의 코드 출력은 아래와 같다.

```
Alice
```

변수는 실제 값을 기억할 필요 없이 프로그램에 데이터를 저장하고 조작할 수 있기 때문에 유용하다. 또한 변수의 값을 언제든지 변경할 수 있으며, 새로운 값은 메모리의 이전 값을 대체할 것이다. 예를 들어, 변수 이름의 값을 `"Alice"`에서 `"Bob"`으로 변경하려면 새로운 값을 할당해야 한다.

```python
# Change the value of the variable name from "Alice" to "Bob"
name = "Bob"

# Print the value of the variable name
print(name)
```

위의 코드 출력은 아래와 같다.

```
Bob
```

변수는 다양한 타입의 데이터를 처리하고 이에 대한 다양한 연산을 수행할 수 있는 복잡하고 동적인 프로그램을 작성하는 데 필수적이다. 다음 절에서는 Python에서 다양한 방법을 사용하여 변수를 선언하는 방법을 배울 것이다.

## Python에서 변수를 선언하는 방법
Python에서 변수를 선언하는 것은 매우 쉽고 간단하다. 변수 타입을 지정하거나 특별한 키워드를 사용할 필요가 없다. 할당 연산자(assignment operator) `=`를 사용하여 변수 이름에 값을 할당하기만 하면 된다. 예를 들어 `age`라는 변수를 만들고 `25`라는 값을 할당할 수 있다.

```python
# Create a variable called age and assign it the value 25
age = 25
```

Python은 변수에 할당하는 값을 기반으로 변수의 타입을 자동으로 추론한다. 이 경우 `age` 변수의 타입은 정수를 나타내는 `int`이다. 내장 함수 `type()`을 사용하여 모든 변수의 타입을 확인할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
# Check the type of the variable age
print(type(age))
```

이 코드의 결과는 다음과 같다.

```
<class 'int'>
```

쉼표로 구분하여 여러 변수를 한 줄에 선언할 수도 있다. 예를 들어 `name`, `city`와 `country`라는 세 변수를 만들고 각각 `"Alice"`, `"New York"`과 `"USA"`라는 값을 할당하여 작성할 수 있다.

```python
# Create three variables in one line
name, city, country = "Alice", "New York", "USA"
```

Python은 줄에 표시된 순서와 동일한 순서로 변수에 값을 할당한다. 변수의 값과 타입을 인쇄하여 확인할 수 있다. 예를 들어 다음과 같이 작성할 수 있다.

```python
# Print the values and types of the variables
print(name, type(name))
print(city, type(city))
print(country, type(country))
```

결과는 다음과 같다.

```
Alice <class 'str'>
New York <class 'str'>
USA <class 'str'>
```

보시시피 `name`, `city`와 `country` 변수의 타입은 문자열을 나타내는 `str`이다. 문자열은 따옴표로 묶인 문자 시퀀스이다. Python에서 문자열을 만들 때는 작은따옴표(`'` `'`) 또는 큰따옴표(`"` `"`)를 사용할 수 있다. 예를 들어 `'Hello'`와 `"Hello"`는 모두 유효한 문자열이다.

Python에서 변수를 선언할 때 따라야 할 몇 가지 규칙과 관습(convention)이 있다. 그것은 다음과 같다.

- 변수 이름은 문자 또는 밑줄(`_`)로 시작해야 한다. 숫자나 다른 기호로 시작할 수 없다.
- 변수 이름에는 문자, 숫자, 밑줄만 포함할 수 있다. 공백이나 다른 문자는 포함할 수 없다.
- 변수 이름은 대소문자를 구분하므로 `name`과 `Name`은 서로 다른 변수이다.
- 변수 이름은 설명적이고 의미 있는 것이어야 하지만 너무 길면 안 된다. 또한 소문자와 밑줄을 사용하여 단어를 구분하도록 권장하는 [PEP 8](https://peps.python.org/pep-0008/) 스타일 가이드를 따라야 한다. 예를 들어, `first_name`은 좋은 변수 이름이지만 `FirstName`이나 `first-name`은 바람직하지 않다.
- 변수 이름은 언어에서 특별한 의미와 기능을 가진 키워드인 Python의 예약어가 아니어야 한다. 예를 들어 `if`, `for`, `print` 또는 `def`는 예약어이므로 변수 이름으로 사용할 수 없다. Python의 [예약어 목록](https://www.w3schools.com/python/python_ref_keywords.asp)을 확인할 수 있다.

이러한 규칙과 관습을 따르면 Python에서 변수를 선언할 때 오류와 혼란을 피할 수 있다. 다음 섹션에서는 변수가 Python에서 저장할 수 있는 다양한 데이터 타입에 대해 알아보겠다.

## 데이터 타입이란?
데이터 타입은 Python에서 변수가 저장할 수 있는 값의 범주이다. 데이터 타입은 값을 표현, 저장, 조작과 연산하는 방법 같은 값의 특성과 동작을 결정한다. Python에는 많은 기본 제공 데이터 타입이 있지만 여기에서는 가장 일반적이고 기본적인 데이터 유형에 초점을 맞추겠다. 타입은 다음과 같다.

- **수**: 정수, 부동 소수점(float) 또는 복소수일 수 있는 수이다. 정수는 `1`, `-5` 또는 `42` 같은 정수이다. 부동 소수점은 `3.14`, `-2.5` 또는 `6.02e23` 같은 십진수이다. 복소수는 `2+3j`, `-1-4j` 또는 `0+1j`같이 실수 부분과 허수 부분이 있는 수이다.
- **문자열**: 문자열은 따옴표로 묶인 문자의 시퀀스이다(예: `"Hello"`, `'Python'` 또는 `"3.14"`). 문자열에는 문자, 숫자, 기호, 공백 또는 기타 모든 문자가 포함될 수 있다. 문자열은 `""` 또는 `''`와 같이 비어 있을 수도 있다.
- **부울(Boolean)**: 부울은 `True` 또는 `False`가 될 수 있는 논리 값이다. 부울은 조건이나 표현식의 값을 나타내는 데 사용된다. 예를 들어 `2 > 1`은 `True`로 평가되는 부울 표현식이고, `2 < 1`은 `False`로 평가되는 부울 표현식이다.
- **리스트(List)**: 리스트는 어떤 데이터 타입이든 값의 정렬된 컬렉션(collection)이다. 목록은 `[1, 2, 3]`, `["a", "b", "c"]` 또는 `[True, False, None]`과 같이 값을 대괄호로 묶어 만들 수 있다. 리스트를 `[]`과 같이 비워 둘 수도 있다. 리스트는 변경 가능하므로 요소(element)를 변경하거나 새 요소를 추가하거나 기존 요소를 제거할 수 있다.
- **(tuple))**: 튜플은 어떤 데이터 타입이든 값의 정렬된 컬렉션이다. 튜플은 리스트와 비슷하지만 값을 괄호로 묶어 `(1, 2, 3)`, `("a", "b", "c")` 또는 `(True, False, None)`과 같이 만들어진다. 튜플은 `()`와 같이 비어 있을 수도 있다. 튜플은 불변이므로 요소를 변경하거나 새 요소를 추가하거나 기존 요소를 제거할 수 없다.
- **딕셔너리(dictionary)**: 딕셔너리는 어떤 데이터 타입이든 키-값 쌍의 정렬되지 않은 컬렉션이다. 딕셔너리는 `{'name': 'Alice', 'age': 25, 'city': 'New York'}`, `{1: 'one', 2: 'two', 3: 'three'}`, 또는 `{True: 'yes', False: 'no', None: 'maybe'}` 같이 중괄호로 키-값 쌍을 묶어 만든다. 딕셔너리는 `{}`와 같이 비어 있을 수도 있다. 딕셔너리는 변경 가능하므로 값을 변경하거나 새 쌍을 추가하거나 기존 쌍을 제거할 수 있다.
- **집합(set)**: 집합는 어떤 데이터 타입이든 고유 값의 정렬되지 않은 컬렉션이다. 집합은 `{1, 2, 3}`, `{'a', 'b', 'c'}` 또는 `{True, False, None}`과 같이 중괄호로 값을 둘러싸서 만든다. 집합은 `set()`같이 비어 있을 수도 있다. 집합은 변경 가능하므로 새 요소를 추가하거나 기존 요소를 제거할 수 있다.

다음은 Python 프로그래밍에서 접하게 될 주요 데이터 타입이다. 각 데이터 타입에는 값을 조작하고 연산하는 데 사용할 수 있는 고유한 메서드와 함수가 있다. 다음 섹션에서는 Python에서 다양한 데이터 타입을 사용하고 이에 대한 몇 가지 기본 연산과 조작을 수행하는 방법을 살펴볼 것이다.

## Python에서 다양한 데이터 타입을 사용하는 방법
이 섹션에서는 Python에서 다양한 데이터 타입을 사용하고 이에 대한 기본 연산과 조작을 수행하는 방법을 알아본다. 또한 각 데이터 타입을 사용하는 방법을 보여주는 몇 가지 코드 예도 볼 수 있을 것이다. 수부터 시작하겠다.

### 수(Number)
수는 Python에서 가장 일반적이고 기본적인 데이터 타입 중 하나이다. 수를 사용하여 더하기, 빼기, 곱하기, 나누기, 지수화 등의 산술 연산을 수행할 수 있다. 또한 수를 사용하여 보다 큼, 보다 작음, 같음 등의 값을 비교할 수도 있다. 다음은 Python에서 수를 사용하는 방법에 대한 몇 가지 예이다.

```python
# Create some numbers
x = 10 # an integer
y = 3.14 # a float
z = 2+5j # a complex number

# Print the numbers and their types
print(x, type(x))
print(y, type(y))
print(z, type(z))

# Perform some arithmetic operations
print(x + y) # addition
print(x - y) # subtraction
print(x * y) # multiplication
print(x / y) # division
print(x ** y) # exponentiation

# Perform some comparisons
print(x > y) # greater than
print(x < y) # less than
print(x == y) # equal to
print(x != y) # not equal to
```

결과는 다음과 같다.

```
0 <class 'int'>
3.14 <class 'float'>
(2+5j) <class 'complex'>
13.14
6.859999999999999
31.400000000000002
3.184713375796178
1380.3842646028852
True
False
False
True
```

보다시피 Python은 다양한 타입의 수를 처리하고 이에 대해 다양한 연산을 수행할 수 있다. 괄호를 사용하여 수학에서와 같이 연산 순서를 변경할 수도 있다. 예를 들어 `(x + y) * z`는 `x + (y * z)`와 다른 결과를 준다.

### 문자열
문자열은 따옴표로 묶인 문자 시퀀스이다. 문자열을 사용하여 이름, 메시지, 제목 등과 같은 텍스트 데이터를 저장하고 조작할 수 있다. 또한 문자열을 사용하여 데이터를 읽기 쉬운 방식으로 서식을 지정하고 표시할 수도 있다. 다음은 Python에서 문자열을 사용하는 방법에 대한 예이다.

```python
# Create some strings
s1 = "Hello" # a string with double quotes
s2 = 'Python' # a string with single quotes
s3 = "" # an empty string
s4 = "3.14" # a string that looks like a number

# Print the strings and their types
print(s1, type(s1))
print(s2, type(s2))
print(s3, type(s3))
print(s4, type(s4))

# Perform some string operations
print(s1 + s2) # concatenation
print(s1 * 3) # repetition
print(s1[0]) # indexing
print(s1[1:3]) # slicing
print(len(s1)) # length
print(s1.upper()) # upper case
print(s2.lower()) # lower case
print(s1.replace("l", "r")) # replacement
print(s4.isdigit()) # check if string is a digit
```

결과는 다음과 같다.

```
Hello <class 'str'>
Python <class 'str'>
 <class 'str'>
3.14 <class 'str'>
HelloPython
HelloHelloHello
H
el
5
HELLO
python
Herro
False
```

보다시피 Python은 다양한 타입의 문자열을 처리하고 다양한 연산을 수행할 수 있다. 새 줄의 경우 `\n`, 탭의 경우 `\t`, 작은따옴표의 경우 `\'` 등 일부 특수 문자를 문자열에 사용할 수도 있다. 예를 들어 `"Hello\nWorld"`는 다음과 같이 인쇄된다.

```
Hello
World
```

`format()` 메서드에서 자리 표시자를 사용하여 문자열에 값을 삽입할 수도 있다. 예를 들어 `“Hello, {}. You are {} years old.”.format(“Alice”, 25)`는 다음과 같이 인쇄된다<sup>[1](#footnote_1)</sup>.

```
Hello, Alice. You are 25 years old.
```

## 마치며
이 포스팅에서는 Python에서 변수를 선언하고 다양한 데이터 타입을 사용하는 방법을 알아보았다. 숫, 문자열, 부울, 리스트, 튜플, 사전 및 집합을 만들고 조작하는 방법을 살펴보았다. 또한 각 데이터 타입에 사용할 수 있는 기본 연산과 함수도 살펴보았다. 또한 각 데이터 타입을 사용하는 방법을 보여주는 코드 예도 보았다.

이러한 개념을 학습함으로써 Python 프로그래밍과 데이터 분석에 대한 탄탄한 기초를 다졌다. 이제 변수와 데이터 타입을 사용하여 프로그램에서 데이터를 저장하고 조작할 수 있다. 또한 이를 사용하여 데이터를 읽기 쉬운 방식으로 포맷하고 디스플레이할 수도 있다. 계산, 비교 및 논리 연산을 수행하는 데도 사용할 수 있다.

<a name="footnote_1">1</a>: [f-string](https://docs.python.org/ko/3/reference/lexical_analysis.html#f-strings)도 참고하세요.
