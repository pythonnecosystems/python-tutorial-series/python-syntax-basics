# Python 구문 기초: 변수, 데이터 타입 <sup>[1](#footnote_1)</sup>

> <font size="3">변수를 선언하고 다양한 데이터 타입을 사용하는 방법에 대해 알아본다.</font>

## 목차
1. [소개](./python-syntax-basics.md#소개)
1. [변수란?](./python-syntax-basics.md#변수란)
1. [Python에서 변수를 선언하는 방법](./python-syntax-basics.md#python에서-변수를-선언하는-방법)
1. [데이터 타입이란?](./python-syntax-basics.md#데이터-타입이란)
1. [Python에서 다양한 데이터 타입을 사용하는 방법](./python-syntax-basics.md#python에서-다양한-데이터-타입을-사용하는-방법)
1. [마치며](./python-syntax-basics.md#마치며)

<a name="footnote_1">1</a>: [Python Tutorial 3 — Python Syntax Basics: Variables, Data Types](https://python.plainenglish.io/python-tutorial-3-python-syntax-basics-variables-data-types-e6750746cb22)를 편역한 것이다.
